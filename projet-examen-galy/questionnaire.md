**NOM Prénom (numéro) : GALY Vincent (09)**


# Traitements


## Quelle est la classe du traitement choisi ?

J'ai choisi d'implémenter le traitement "Doublon" afin de ne pas avoir deux fois une même position dans la liste.


## Pourquoi avoir fait de traitement une classe abstraite et non une interface ?

Le fait que Traitement soit une classe abstraite permet d'avoir des méthodes comme *traiter()*, *gererFinLot()*, etc... avec un comportement par défaut implémentable par tous ceux héritant de cette dernière.


## Pourquoi certaines méthodes sont déclarées `final` ?

Certaines méthodes sont déclarées `final` afin que les classes enfants implémentant ces dernières ne puissent pas les surchargées et qu'ils conservent donc leur comportement initial.


## La classe Traitement est abstraite alors qu'elle ne contient pas de méthodes abstraites. Pourquoi ?

La classe abstraite Traitement permet d'assurer le chaînage entre les traitements, cela oblige l'utilisateur à faire hériter de cette dernière afin d'unifier le comportement des nouveaux traitements.


## Est-ce que de faire de Traitement une classe abstraite est vraiment logique ici ?

Le fait d'utiliser une classe abstraite force le développeur à implémenter certaines méthodes ayant un comportement similaire, unifiant donc le fonctionnement global des Traitement. Faire de cette dernière une classe abstraite est donc logique.

## Pour le traitement Normaliseur, quels autres traitements avez-vous utilisés et comment ?

Pour le traitement Normaliseur, j'ai utilisé deux traitements *Max* (un pour pour le Max et l'autre pour le Min comme expliqué dans le sujet) ainsi que le traitement *Donnees* afin de récupérer les données traitées.


## Quelles modifications avez-vous été obligés de faire sur la classe Position ?

Afin de comparer x et y dans *frequence()*, il a fallu *Override* la méthode **equals()** afin de comparer deux positions entre-elles *(pos1.x == pos2.x && pos1.y == pos2.y)*.



# Remarques sur Swing

Afin de créer l'interface Swing, j'ai construit cette dernière dans la fonction *panelContent()* retournant le contenu du panel principal utilisé par la JFrame. J'ai implémenté l'interface **ActionListener** et *Override* la méthode *actionPerformed()* afin de connaitre la source du clique et d'effectuer les bonnes actions suivant le bouton.


# Remarques sur l'Introspection

- Class.forName(): Ne fonctionne pas sur les types primitifs (int, double, float...), pour cela, j'ai dû renvoyer manuellement les types **int.class**, **double.class** quand une exception *ClassNotFoundException* était levée.


# Remarques sur XML

## Lecture d'un document XML

Afin de parser et lire toutes les données d'un document XML, j'utilise la bibliothèque JDOM2.


## Production d'un document XML

Je récupère les données (positions et valeurs) via les listes dans le traitement *Donnees* et utilise la lib JDOM2 afin de générer le fichier.


# Principaux choix faits

J'ai choisi d'utiliser le traitement *Donnees* afin de récupérer les valeurs dans la classe **GenerateurXML** ou encore **Normaliseur** afin d'avoir tout centralisé à un seul endroit et éviter les redondances.


# Critiques de l'architecture proposée et améliorations possibles

L'architecture proposée est intéressante mais une mise en contexte aurait peut-être été meilleur pour la compréhension globale de ce qui était demandé (ex: application banque).


# Difficultés rencontrées

Difficultés à comprendre ce qui était attendu par moments.

