package fr.n7.cnam.nfp121.pr01;

import java.util.ArrayList;

/**
  * Donnees enregistre toutes les données reçues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Donnees extends Traitement {

	private ArrayList<Position> positionList;
	private ArrayList<Double> doubleList;

	public Donnees() {
		this.positionList = new ArrayList<Position>();
		this.doubleList = new ArrayList<Double>(); 
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.positionList.add(position);
		this.doubleList.add(valeur);
		super.traiter(position, valeur);
	}
	
	public ArrayList<Position> getPositionList() {
		return this.positionList;
	}
	
	public ArrayList<Double> getDoubleList() {
		return this.doubleList;
	}
	
}
