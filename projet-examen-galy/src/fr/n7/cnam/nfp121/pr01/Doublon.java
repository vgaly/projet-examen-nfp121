package fr.n7.cnam.nfp121.pr01;

public class Doublon extends Traitement {
	
	private Donnees donnees;
	
	@Override
	public void gererDebutLotLocal(String nomLot)
	{
		this.donnees = new Donnees();
		this.donnees.gererDebutLot(nomLot);
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		if (position == null) {
			return;
		}
		
		if (!this.donnees.getPositionList().contains(position)) {
			this.donnees.getPositionList().add(position);
		} else {
			System.out.println("Doublon détecté: " + position.toString() + " - " + valeur);
		}

		super.traiter(position, valeur);
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		this.donnees.gererFinLot(nomLot);
	}
	
}
