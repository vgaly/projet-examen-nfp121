package fr.n7.cnam.nfp121.pr01;

/**
  * SommeParPosition 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class SommeParPosition extends Traitement {

	private Donnees donnees;
	
	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.donnees = new Donnees();
		this.donnees.gererDebutLotLocal(nomLot);
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		if (position == null) {
			return;
		}
		
		if(this.donnees.getPositionList().contains(position)) {
			int index = this.donnees.getPositionList().indexOf(position);
			this.donnees.getDoubleList().set(index, this.donnees.getDoubleList().get(index) + valeur);
		} else {
			this.donnees.traiter(position, valeur);
		}
				
		super.traiter(position, valeur);
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println("Somme par position pour le lot " + nomLot + ": ");
		
		for(int i = 0; i < this.donnees.getPositionList().size(); i++) {
			System.out.println("La Position: " + this.donnees.getPositionList().get(i) + " a pour valeur: " + this.donnees.getDoubleList().get(i));
		}
	}
}
