package fr.n7.cnam.nfp121.pr01;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SaisiesSwing extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private JFrame curWindow;
	private JPanel mainPanel;
	private JTextField x;
	private JTextField y;
	private JTextField valeur;
	private JButton validerButton;
	private JButton effacerButton;
	private JButton terminerButton;

	private List<Integer> listX;
	private List<Integer> listY;
	private List<Double> listValeur;
	
	private String fileName;
	
	public static void main(String[] args) {
		if(args.length < 1) {
			throw new IllegalArgumentException("Vous devez écrire un nom de fichier en argumant");
		}
		new SaisiesSwing(args[0]);
	}

	public SaisiesSwing(String fileName) {
		this.fileName = fileName;
		
		this.listX = new ArrayList<Integer>();
		this.listY = new ArrayList<Integer>();
		this.listValeur = new ArrayList<Double>();
		this.curWindow = new JFrame("Saisie données");

		this.curWindow.setResizable(false);
		this.curWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.curWindow.setContentPane(this.panelContent());
		this.curWindow.setVisible(true);
		this.curWindow.pack();
	}
	
	public JPanel panelContent() {
		
		this.mainPanel = new JPanel();
		this.mainPanel.setLayout(new GridLayout(3, 1));
		
		/*** Text ***/
		Container textContainer = new Container();
		textContainer.setLayout(new GridLayout(1, 3));
		textContainer.add(new JLabel("Abscisse", JLabel.CENTER));
		textContainer.add(new JLabel("Ordonnée", JLabel.CENTER));
		textContainer.add(new JLabel("Valeur", JLabel.CENTER));

		/*** TextBox ***/
		Container fieldContainer = new Container();
		fieldContainer.setLayout(new GridLayout(1, 3));
		this.x = new JTextField();
		this.x.setPreferredSize(new Dimension(150, 7));
		this.y = new JTextField();
		this.valeur = new JTextField();
		fieldContainer.add(this.x);
		fieldContainer.add(this.y);
		fieldContainer.add(this.valeur);

		/*** Buttons ***/
		Container buttonContainer = new Container();
		buttonContainer.setLayout(new GridLayout(1, 3));
		this.validerButton = new JButton("Valider");
		this.effacerButton = new JButton("Effacer");
		this.terminerButton = new JButton("Terminer");
		this.validerButton.addActionListener(this);
		this.effacerButton.addActionListener(this);
		this.terminerButton.addActionListener(this);

		buttonContainer.add(this.validerButton);
		buttonContainer.add(this.effacerButton);
		buttonContainer.add(this.terminerButton);

		/*** Add everything to mainPanel ***/
		this.mainPanel.add(textContainer);
		this.mainPanel.add(fieldContainer);
		this.mainPanel.add(buttonContainer);

		return this.mainPanel;
	}

	private boolean isDouble(String valueToCheck) {
		if(!valueToCheck.contains(".")) {
		    return false;
		}
		
		try {
			Double.parseDouble(valueToCheck);
		} catch (NumberFormatException e) {
			return false;
		}
		
		return true;
	}
	
	private boolean isInt(String valueToCheck) {
		try {
			Integer.parseInt(valueToCheck);
		} catch (NumberFormatException e) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		
		if(source == this.validerButton) {
			if(!this.isInt(this.x.getText())) {
				this.x.setBackground(Color.RED);
				return;
			}
			
			if(!this.isInt(this.y.getText())) {
				this.y.setBackground(Color.RED);
				return;
			}
			
			if(!this.isDouble(this.valeur.getText())) {
				this.valeur.setBackground(Color.RED);
				return;
			}
			
			this.x.setBackground(Color.WHITE);
			this.y.setBackground(Color.WHITE);
			this.valeur.setBackground(Color.WHITE);
			
			this.listX.add(Integer.parseInt(this.x.getText()));
			this.listY.add(Integer.parseInt(this.y.getText()));
			this.listValeur.add(Double.parseDouble(this.valeur.getText()));
		} else if(source == this.effacerButton) {
			this.x.setText("");
			this.y.setText("");
			this.valeur.setText("");
			
			this.x.setBackground(Color.WHITE);
			this.y.setBackground(Color.WHITE);
			this.valeur.setBackground(Color.WHITE);
		} else if(source == this.terminerButton) {
			System.out.println("Voici les données allant être enregistrées dans " + this.fileName);
			System.out.println(Arrays.toString(this.listX.toArray()));
			System.out.println(Arrays.toString(this.listY.toArray()));
			System.out.println(Arrays.toString(this.listValeur.toArray()));
			
			FileWriter fileWriter;
			try {
				fileWriter = new FileWriter(this.fileName, true);
				for(int i = 0; i < this.listValeur.size(); i++) {
					int identifiantLigne = i + 1;
					fileWriter.write(identifiantLigne + " " + this.listX.get(i) + " " + this.listY.get(i) + " " + this.listValeur.get(i) + "\n");
				}
				fileWriter.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
		}
	}
	
}
