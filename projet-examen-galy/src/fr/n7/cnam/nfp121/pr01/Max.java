package fr.n7.cnam.nfp121.pr01;

/**
  * Max calcule le max des valeurs vues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Max extends Traitement {

	private double max = Double.NEGATIVE_INFINITY;
	
	@Override
	public void traiter(Position position, double valeur) {
		if(valeur > max) {
			this.max = valeur;
		}
		super.traiter(position, valeur);
	}
	
	public double max() {
		return this.max;
	}

}
