package fr.n7.cnam.nfp121.pr01;

/**
  * Normaliseur normalise les données d'un lot en utilisant une transformation affine.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Normaliseur extends Traitement {
	
	private double debut = Double.NEGATIVE_INFINITY;
	private double fin = Double.POSITIVE_INFINITY;
	private Donnees donnees;
	private Max max;
	private Max min;

	public Normaliseur(double debut, double fin) {
		this.debut = debut;
		this.fin = fin;
		this.max = new Max();
		this.min = new Max();
		this.donnees = new Donnees();
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		
		// this donne traitee
		// overide dans normaliseur gererDebutLotLocal (this donner gererdebutlot)/gererFinLotLocal, dans fin lot, faire tout traitement sur les max
		// supprimer les super.traiter()
		// getter acceder aux listes (donnees)

		this.donnees.traiter(position, valeur);
		this.max.traiter(position, valeur);
		this.min.traiter(position, -valeur);

		super.traiter(position, valeur);
	}
	
	@Override
	protected void gererDebutLotLocal(String nomLot) {
		this.donnees.gererDebutLot("normalise");
	}

	@Override
	protected void gererFinLotLocal(String nomLot) {
		// on calcul le max etc ici avec la liste dans donnes
		this.donnees.gererFinLot("normalise");
		double max = this.max.max();
		double min = -this.min.max();

		double a = (max - min) / (this.fin - this.debut);
		double b = this.debut - a * min;

		for (int i = 0; i < this.donnees.getDoubleList().size(); i++) {
			this.donnees.getDoubleList().set(i, a * this.donnees.getDoubleList().get(i) + b);
		}
	}
	
}
