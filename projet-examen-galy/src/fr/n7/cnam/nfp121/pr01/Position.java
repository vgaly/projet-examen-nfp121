package fr.n7.cnam.nfp121.pr01;

/** Définir une position.  */
public class Position {
	public int x;
	public int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
		// System.out.println("...appel à Position(" + x + "," + y + ")" + " --> " + this);
	}

	@Override
	public String toString() {
		return super.toString() + "(" + this.x + "," + this.y + ")";
	}
	
	@Override
	public boolean equals(Object object) {
		if(object == null) {
			return false;
		}

		Position posObject = (Position)object;
		return (this.x == posObject.x && this.y == posObject.y);
	}

}
